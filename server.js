const express = require('express');
const mongoose = require ('mongoose');
const Screen = require('./model');
const cors = require ('cors');
const app = express();
app.use(express.json());
mongoose.connect('mongodb+srv://m001-student:soumya@sandbox.lyukn.mongodb.net/myFirstDatabase?retryWrites=true&w=majority').then(
() => console.log('DB connected')
).catch(err => console.log(err))

app.post('/screen', async(req,res) => {
  const {screenName,numberOfSeats,time} = req.body;
     try{
     const newScreen = new Screen ({screenName,numberOfSeats,time});
     await newScreen.save();
     return res.json(await Screen.find());
}
catch (err){
  console.log(err.message);
}
});

app.get('/screendetails', async(req,res) => {

  try{
const alldata = await Screen.find();
return res.json(alldata);

  }
  catch(err){
    console.log(err.message);
  }
});

app.listen(3000,()=>{
    console.log('server is running');
})