const mongoose = require('mongoose');

const Screen = mongoose.Schema({

    numberOfSeats : {
        type : Number,
        required : true,
        
    },
    screenName : {
        type : String,
        required : true,
    },
    time : {
        type : String,
        required : true,
    },
    date : {
        type : Date,
        default : Date.now,
    }
    })
    
module.exports = mongoose.model('ScreenName,numberOfSeats,time',Screen);